ewoksmx |version|
=================

*ewoksmx* provides beamline automation workflows for MX.

*ewoksmx* has been developed by the `Software group <http://www.esrf.eu/Instrumentation/software>`_
of the `European Synchrotron <https://www.esrf.eu/>`_.

Documentation
-------------

.. toctree::
    :hidden:

    api
