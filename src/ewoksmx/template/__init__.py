import sys

if sys.version_info < (3, 9):
    import importlib_resources
else:
    import importlib.resources as importlib_resources

import jinja2


def get_rendered_template(template_name: str, variables: dict) -> str:
    """The resource is specified relative to `blissoda.resources`.

    .. code-block:: python

        file_content = get_rendered_template("edna_script.py", {...})
    """
    source = importlib_resources.files(__name__) / f"{template_name}.j2"
    with importlib_resources.as_file(source) as path:
        if not path.is_file():
            raise FileNotFoundError(f"No template with the name '{template_name}'")

        env = jinja2.Environment(autoescape=False, undefined=jinja2.StrictUndefined)

        with open(path) as f:
            template = env.from_string(f.read())

        return template.render(**variables)
