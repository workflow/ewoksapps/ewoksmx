from typing import List, Optional

from pydantic import Field

from .common import XSData
from .common import XSDataFloat
from .common import XSDataInteger
from .common import XSDataBoolean
from .common import XSDataString
from .common import XSDataFile
from .common import XSDataRange


class XSDataInputControlAutoPROC(XSData):
    """Data model for AutoPROC.

    https://www.globalphasing.com/autoproc/
    """

    dataCollectionId: Optional[XSDataInteger] = Field(None, alias="MX_dataCollectionId")
    processDirectory: Optional[XSDataFile] = Field(None)
    icatProcessDataDir: Optional[XSDataFile] = Field(None)
    fromN: Optional[XSDataInteger] = Field(None, alias="start_image")
    toN: Optional[XSDataInteger] = Field(None, alias="end_image")
    doAnom: Optional[XSDataBoolean] = Field(
        default=False, validate_default=True, alias="anomalous"
    )
    symm: Optional[XSDataString] = Field(None, alias="forced_spacegroup")
    reprocess: Optional[XSDataBoolean] = Field(default=True, validate_default=True)
    lowResolutionLimit: Optional[XSDataFloat] = Field(None, alias="low_res_limit")
    highResolutionLimit: Optional[XSDataFloat] = Field(None, alias="high_res_limit")
    exclude_range: List[XSDataRange] = Field(None)
