from typing import List, Optional

from pydantic import Field

from .common import XSData
from .common import XSDataFloat
from .common import XSDataInteger
from .common import XSDataBoolean
from .common import XSDataString
from .common import XSDataFile
from .common import XSDataRange


class XSDataEDNAprocInput(XSData):
    """Data model for EDNA fast processing.

    https://www.esrf.fr/UsersAndScience/Experiments/MX/How_to_use_our_beamlines/Run_Your_Experiment/edna-autoprocessing
    """

    input_file: Optional[XSDataFile] = Field(..., alias="xds_inp_path")
    output_file: Optional[XSDataFile] = Field(..., alias="ispyb_xml_path")
    res_override: Optional[XSDataFloat] = Field(None, alias="high_res_limit")
    data_collection_id: Optional[XSDataInteger] = Field(
        None, alias="MX_dataCollectionId"
    )
    icat_processed_data_dir: Optional[XSDataString] = Field(
        None, alias="icatProcessDataDir"
    )
    low_resolution_limit: Optional[XSDataFloat] = Field(None, alias="low_res_limit")
    reprocess: Optional[XSDataBoolean] = Field(default=True, validate_default=True)
    spacegroup: Optional[XSDataString] = Field(None, alias="forced_spacegroup")
    start_image: Optional[XSDataInteger] = Field(None, alias="start_image")
    end_image: Optional[XSDataInteger] = Field(None, alias="start_image")
    exclude_range: List[XSDataRange] = Field(None, alias="exclude_range")
    doAnom: Optional[XSDataBoolean] = Field(
        default=False, validate_default=True, alias="anomalous"
    )
