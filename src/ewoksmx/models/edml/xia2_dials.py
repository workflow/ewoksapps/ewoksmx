from typing import List, Optional

from pydantic import Field

from .common import XSData
from .common import XSDataFloat
from .common import XSDataInteger
from .common import XSDataBoolean
from .common import XSDataString
from .common import XSDataFile
from .common import XSDataRange


class XSDataInputControlXia2DIALS(XSData):
    """Data model for the XIA2 DIALS pipeline.

    https://xia2.github.io/installation.html
    """

    dataCollectionId: Optional[XSDataInteger] = Field(None, alias="MX_dataCollectionId")
    processDirectory: Optional[XSDataFile] = Field(None)
    icatProcessDataDir: Optional[XSDataFile] = Field(None, alias="icatProcessDataDir")
    doAnom: Optional[XSDataBoolean] = Field(
        default=False, validate_default=True, alias="anomalous"
    )
    spaceGroup: Optional[XSDataString] = Field(None, alias="forced_spacegroup")
    startFrame: Optional[XSDataInteger] = Field(None, alias="start_image")
    endFrame: Optional[XSDataInteger] = Field(None, alias="end_image")
    exclude_range: List[XSDataRange] = Field(None, alias="exclude_range")
    reprocess: Optional[XSDataBoolean] = Field(default=True, validate_default=True)
    lowResolutionLimit: Optional[XSDataFloat] = Field(None, alias="low_res_limit")
    highResolutionLimit: Optional[XSDataFloat] = Field(None, alias="high_res_limit")
