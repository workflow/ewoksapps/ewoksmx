from ewoksmx.models.edml.xia2_dials import XSDataInputControlXia2DIALS
from .utils import model_validation


def test_xia2_dials():
    workflow_parameters = {
        "MX_dataCollectionId": 123456,
        "forced_spacegroup": "P1",
        "low_res_limit": 4.0,
        "high_res_limit": 1.0,
        "start_image": 10,
        "end_image": 990,
        "exclude_range": [(10, 20), (30, 40)],
        "processDirectory": "/tmp/nobackup",
        "icatProcessDataDir": "/tmp",
    }

    expected_edml = {
        "dataCollectionId": {"value": 123456},
        "doAnom": {"value": False},
        "endFrame": {"value": 990},
        "exclude_range": [{"begin": 10, "end": 20}, {"begin": 30, "end": 40}],
        "highResolutionLimit": {"value": 1.0},
        "processDirectory": {"path": {"value": "/tmp/nobackup"}},
        "icatProcessDataDir": {"path": {"value": "/tmp"}},
        "lowResolutionLimit": {"value": 4.0},
        "reprocess": {"value": True},
        "spaceGroup": {"value": "P1"},
        "startFrame": {"value": 10},
    }

    expected_xml = """<?xml version="1.0" encoding="utf-8"?>
<XSDataInputControlXia2DIALS>
  <dataCollectionId>
    <value>123456</value>
  </dataCollectionId>
  <processDirectory>
    <path>
      <value>/tmp/nobackup</value>
    </path>
  </processDirectory>
  <icatProcessDataDir>
    <path>
      <value>/tmp</value>
    </path>
  </icatProcessDataDir>
  <doAnom>
    <value>false</value>
  </doAnom>
  <spaceGroup>
    <value>P1</value>
  </spaceGroup>
  <startFrame>
    <value>10</value>
  </startFrame>
  <endFrame>
    <value>990</value>
  </endFrame>
  <exclude_range>
    <begin>10</begin>
    <end>20</end>
  </exclude_range>
  <exclude_range>
    <begin>30</begin>
    <end>40</end>
  </exclude_range>
  <reprocess>
    <value>true</value>
  </reprocess>
  <lowResolutionLimit>
    <value>4.0</value>
  </lowResolutionLimit>
  <highResolutionLimit>
    <value>1.0</value>
  </highResolutionLimit>
</XSDataInputControlXia2DIALS>"""

    model_validation(
        XSDataInputControlXia2DIALS, workflow_parameters, expected_edml, expected_xml
    )
