from .utils import model_validation

from ewoksmx.models.edml import common


def test_empty():
    data = expected_edml = {}
    expected_edml = {}
    expected_xml = """<?xml version="1.0" encoding="utf-8"?>
<XSData></XSData>"""
    model_validation(common.XSData, data, expected_edml, expected_xml)


def test_boolean():
    data = expected_edml = {"value": True}
    expected_xml = """<?xml version="1.0" encoding="utf-8"?>
<XSDataBoolean>
  <value>true</value>
</XSDataBoolean>"""
    model_validation(common.XSDataBoolean, data, expected_edml, expected_xml)


def test_integer():
    data = expected_edml = {"value": 10}
    expected_xml = """<?xml version="1.0" encoding="utf-8"?>
<XSDataInteger>
  <value>10</value>
</XSDataInteger>"""
    model_validation(common.XSDataInteger, data, expected_edml, expected_xml)
    model_validation(common.XSDataInteger, {"value": "10"}, expected_edml, expected_xml)


def test_float():
    data = expected_edml = {"value": 10.1}
    expected_xml = """<?xml version="1.0" encoding="utf-8"?>
<XSDataFloat>
  <value>10.1</value>
</XSDataFloat>"""
    model_validation(common.XSDataFloat, data, expected_edml, expected_xml)
    model_validation(common.XSDataFloat, {"value": "10.1"}, expected_edml, expected_xml)


def test_string():
    data = expected_edml = {"value": "test string"}
    expected_xml = """<?xml version="1.0" encoding="utf-8"?>
<XSDataString>
  <value>test string</value>
</XSDataString>"""
    model_validation(common.XSDataString, data, expected_edml, expected_xml)


def test_range():
    data = expected_edml = {"begin": 10, "end": 20}
    expected_xml = """<?xml version="1.0" encoding="utf-8"?>
<XSDataRange>
  <begin>10</begin>
  <end>20</end>
</XSDataRange>"""
    model_validation(common.XSDataRange, data, expected_edml, expected_xml)
    model_validation(
        common.XSDataRange, {"begin": "10", "end": "20"}, expected_edml, expected_xml
    )


def test_file():
    data = {"path": "/tmp"}
    expected_edml = {"path": {"value": "/tmp"}}
    expected_xml = """<?xml version="1.0" encoding="utf-8"?>
<XSDataFile>
  <path>
    <value>/tmp</value>
  </path>
</XSDataFile>"""
    model_validation(common.XSDataFile, data, expected_edml, expected_xml)
