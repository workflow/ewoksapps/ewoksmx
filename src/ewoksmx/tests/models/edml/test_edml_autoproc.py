from ewoksmx.models.edml.autoproc import XSDataInputControlAutoPROC
from .utils import model_validation


def test_auto_proc():
    workflow_parameters = {
        "MX_dataCollectionId": 123456,
        "forced_spacegroup": "P1",
        "anomalous": True,
        "low_res_limit": 4.0,
        "high_res_limit": 1.0,
        "start_image": 10,
        "end_image": 990,
        "exclude_range": [(10, 20), (30, 40)],
        "processDirectory": "/tmp/nobackup",
        "icatProcessDataDir": "/tmp",
    }

    expected_edml = {
        "dataCollectionId": {"value": 123456},
        "doAnom": {"value": True},
        "exclude_range": [{"begin": 10, "end": 20}, {"begin": 30, "end": 40}],
        "fromN": {"value": 10},
        "highResolutionLimit": {"value": 1.0},
        "processDirectory": {"path": {"value": "/tmp/nobackup"}},
        "icatProcessDataDir": {"path": {"value": "/tmp"}},
        "lowResolutionLimit": {"value": 4.0},
        "reprocess": {"value": True},
        "symm": {"value": "P1"},
        "toN": {"value": 990},
    }

    expected_xml = """<?xml version="1.0" encoding="utf-8"?>
<XSDataInputControlAutoPROC>
  <dataCollectionId>
    <value>123456</value>
  </dataCollectionId>
  <processDirectory>
    <path>
      <value>/tmp/nobackup</value>
    </path>
  </processDirectory>
  <icatProcessDataDir>
    <path>
      <value>/tmp</value>
    </path>
  </icatProcessDataDir>
  <fromN>
    <value>10</value>
  </fromN>
  <toN>
    <value>990</value>
  </toN>
  <doAnom>
    <value>true</value>
  </doAnom>
  <symm>
    <value>P1</value>
  </symm>
  <reprocess>
    <value>true</value>
  </reprocess>
  <lowResolutionLimit>
    <value>4.0</value>
  </lowResolutionLimit>
  <highResolutionLimit>
    <value>1.0</value>
  </highResolutionLimit>
  <exclude_range>
    <begin>10</begin>
    <end>20</end>
  </exclude_range>
  <exclude_range>
    <begin>30</begin>
    <end>40</end>
  </exclude_range>
</XSDataInputControlAutoPROC>"""

    model_validation(
        XSDataInputControlAutoPROC, workflow_parameters, expected_edml, expected_xml
    )
