from pprint import pprint
from typing import Type, Any

from ewoksmx.models.edml.common import XSData


def model_validation(
    model_cls: Type[XSData],
    data: Any,
    expected_edml: dict,
    expected_xml: str,
):
    if isinstance(data, dict):
        model = model_cls(**data)
    else:
        model = model_cls(data)

    # Test that the EDML dict has the expected value
    edml_dict = model.to_edml_dict()
    pprint(edml_dict)
    assert edml_dict == expected_edml

    # Test that the XML string has the expected value
    xml_string = model.to_xml_string()
    print(xml_string)
    assert xml_string == expected_xml

    # Pydantic round-trip
    converted = model_cls(**model.model_dump())
    assert model == converted

    # EDML dict round-trip
    converted = model_cls.from_edml_dict(model.to_edml_dict())
    assert model == converted

    # EDML XML string round-trip
    converted = model_cls.from_xml_string(model.to_xml_string())
    assert model == converted
