from ewoksmx.models.edml.edna_proc import XSDataEDNAprocInput
from .utils import model_validation


def test_edna_proc():
    workflow_parameters = {
        "MX_dataCollectionId": 123456,
        "forced_spacegroup": "P1",
        "anomalous": True,
        "low_res_limit": 4.0,
        "high_res_limit": 1.0,
        "start_image": 10,
        "end_image": 990,
        "icatProcessDataDir": "/tmp",
        "exclude_range": [[10, 20], [30, 40]],
        "reprocess": True,
        "xds_inp_path": "/tmp/XDS.INP",
        "ispyb_xml_path": "/tmp/nobackup/ispyb.xml",
    }

    expected_edml = {
        "data_collection_id": {"value": 123456},
        "doAnom": {"value": True},
        "end_image": {"value": 10},
        "exclude_range": [{"begin": 10, "end": 20}, {"begin": 30, "end": 40}],
        "icat_processed_data_dir": {"value": "/tmp"},
        "input_file": {"path": {"value": "/tmp/XDS.INP"}},
        "output_file": {"path": {"value": "/tmp/nobackup/ispyb.xml"}},
        "low_resolution_limit": {"value": 4.0},
        "reprocess": {"value": True},
        "res_override": {"value": 1.0},
        "spacegroup": {"value": "P1"},
        "start_image": {"value": 10},
    }

    expected_xml = """<?xml version="1.0" encoding="utf-8"?>
<XSDataEDNAprocInput>
  <input_file>
    <path>
      <value>/tmp/XDS.INP</value>
    </path>
  </input_file>
  <output_file>
    <path>
      <value>/tmp/nobackup/ispyb.xml</value>
    </path>
  </output_file>
  <res_override>
    <value>1.0</value>
  </res_override>
  <data_collection_id>
    <value>123456</value>
  </data_collection_id>
  <icat_processed_data_dir>
    <value>/tmp</value>
  </icat_processed_data_dir>
  <low_resolution_limit>
    <value>4.0</value>
  </low_resolution_limit>
  <reprocess>
    <value>true</value>
  </reprocess>
  <spacegroup>
    <value>P1</value>
  </spacegroup>
  <start_image>
    <value>10</value>
  </start_image>
  <end_image>
    <value>10</value>
  </end_image>
  <exclude_range>
    <begin>10</begin>
    <end>20</end>
  </exclude_range>
  <exclude_range>
    <begin>30</begin>
    <end>40</end>
  </exclude_range>
  <doAnom>
    <value>true</value>
  </doAnom>
</XSDataEDNAprocInput>"""

    model_validation(
        XSDataEDNAprocInput, workflow_parameters, expected_edml, expected_xml
    )
