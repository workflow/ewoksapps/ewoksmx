import logging
from unittest import mock
from typing import Optional

import requests

from ewoksmx.tasks.base_tasks.icat_task import IcatCallbackTask


def test_icat_callback_task(caplog):
    payload = {"dummy": None}
    task = _Dummy({"callback_url": None, "payload": payload})

    with mock.patch.object(requests, "put", autospec=True) as mock_put:
        with caplog.at_level(logging.INFO):
            task.execute()

        assert f"Skip notification to ICAT (no callback URL): {payload}" in caplog.text

        mock_put.assert_not_called()


def test_icat_callback_task_with_url(caplog):
    payload = {"dummy": None}
    task = _Dummy(inputs={"callback_url": "http://[MASKED]", "payload": payload})

    with mock.patch.object(requests, "put", autospec=True) as mock_put:
        with caplog.at_level(logging.INFO):
            task.execute()

        mock_put.assert_called_once_with("http://[MASKED]", json=payload)

        assert f"Send notification to ICAT: {payload}" in caplog.text


class _Dummy(IcatCallbackTask, input_names=["callback_url", "payload"]):

    def run(self):
        self.notify_icat(self.inputs.payload)

    @property
    def _icat_callback_url(self) -> Optional[str]:
        return self.inputs.callback_url
