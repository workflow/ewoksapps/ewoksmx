import pathlib
from unittest.mock import patch

import pytest

from ..data import resource_path_context
from ewoksmx.tasks.Grenades_fastproc_pipeline import Grenades_fastproc_pipeline


@pytest.fixture()
def xds_inp_path():
    with resource_path_context("XDS.INP") as xds_inp_path:
        yield xds_inp_path


@pytest.fixture
def ensure_get_spg_and_cell_params():
    with patch.object(
        Grenades_fastproc_pipeline, "_get_spg_and_cell_params"
    ) as mock_method:
        mock_method.return_value = (1, [77.5, 155.8, 155.9, 60.1, 89.9, 89.8])
        yield


def test_grenades_fastproc_prepare(
    tmp_path: pathlib.Path, xds_inp_path: pathlib.Path, ensure_get_spg_and_cell_params
):
    metadata = {
        "beamline": "id23eh2",
        "proposal": "20231212",
        "MX_dataCollectionId": 123456,
        "reprocess_path": str(tmp_path),
        "xds_inp_path": str(xds_inp_path),
        "MX_directory": str(tmp_path / "mx_directory"),
        "forced_spacegroup": "P1",
        "exclude_range": [[10, 20], [30, 40]],
        "no_pipelines": 1,
    }
    pipeline = Grenades_fastproc_pipeline(
        inputs={"metadata": metadata, "grenades_fastproc": True}
    )
    pipeline.execute()

    assert pipeline.xds_inp_path.exists()
    _validate_xds_inp(pipeline.xds_inp_path, metadata)

    for dcolid_path in pipeline.dcloid_file_paths:
        assert dcolid_path.exists()
        _validate_dcolid(dcolid_path, metadata)

    assert pipeline.script_file_path.exists()

    expected = {
        "slurm_params": {
            "script_file_path": str(
                tmp_path / "grenades_fastproc" / "nobackup" / "grenades_fastproc.sh"
            ),
            "queue": "mx",
            "mem": 16000,
            "nodes": 1,
            "core": 20,
            "time": "2:00:00",
            "icat_dir": str(tmp_path / "grenades_fastproc"),
            "icat_callback_url": None,
            "no_pipelines": 1,
            "pipeline_name": "grenades_fastproc",
            "error_message": None,
        },
        "pipeline_name": "grenades_fastproc",
    }
    assert pipeline.get_output_values() == expected


def _validate_xds_inp(xds_inp_path: pathlib.Path, metadata: dict):
    with open(xds_inp_path) as f:
        xds_inp = f.read()

    assert f"NAME_TEMPLATE_OF_DATA_FRAMES={metadata['MX_directory']}" in xds_inp

    for begin, end in metadata["exclude_range"]:
        assert f"EXCLUDE_DATA_RANGE= {begin} {end}" in xds_inp

    assert "SPACE_GROUP_NUMBER= 1" in xds_inp
    assert "UNIT_CELL_CONSTANTS= 77.5 155.8 155.9 60.1 89.9 89.8" in xds_inp


def _validate_dcolid(dcolid_path: pathlib.Path, metadata: dict):
    with open(dcolid_path) as f:
        dcolid = f.read()
    assert dcolid == f"datacollectionID:{metadata['MX_dataCollectionId']}\n"
