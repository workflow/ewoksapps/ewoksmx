from ewoksmx.tasks.XIA2_DIALS_pipeline import XIA2_DIALS_pipeline


def test_xia2_dials_prepare(tmp_path):
    metadata = {
        "beamline": "id30a2",
        "proposal": "mx2112",
        "MX_dataCollectionId": 123456,
        "reprocess_path": str(tmp_path),
        "no_pipelines": 1,
    }
    pipeline = XIA2_DIALS_pipeline(inputs={"metadata": metadata, "XIA2_DIALS": True})
    pipeline.execute()

    assert pipeline.input_file_path.exists()
    with open(pipeline.input_file_path) as f:
        input_xml = f.read()
    print(input_xml)
    assert pipeline.script_file_path.exists()

    expected = {
        "slurm_params": {
            "script_file_path": str(
                tmp_path / "XIA2_DIALS" / "nobackup" / "edna_script.py"
            ),
            "queue": "mx",
            "mem": 96000,
            "nodes": 1,
            "core": 20,
            "time": "2:00:00",
            "icat_dir": str(tmp_path / "XIA2_DIALS"),
            "icat_callback_url": None,
            "no_pipelines": 1,
            "pipeline_name": "XIA2_DIALS",
            "error_message": None,
        },
        "pipeline_name": "XIA2_DIALS",
    }
    assert pipeline.get_output_values() == expected
