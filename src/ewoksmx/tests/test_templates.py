import pytest

from ewoksmx.template import get_rendered_template


def test_not_existing():
    with pytest.raises(
        FileNotFoundError, match="No template with the name 'not_existing'"
    ):
        _ = get_rendered_template("not_existing", {})


def test_edna_script():
    variables = {
        "beamline": "id30a2",
        "proposal": "mx2112",
        "scriptDir": "/tmp/nobackup",
        "dataCollectionId": 123456,
        "inputFile": "/tmp/autoPROC_input.xml",
        "ispybUserName": "[MASKED]",
        "ispybPassword": "[MASKED]",
        "EDNA_SITE": "ESRF_ID30A2",
        "pluginName": "EDPluginControlAutoPROCv1_0",
        "name": "autoPROC",
        "residues": 200,
        "workingDir": "/tmp/nobackup",
    }

    file_content = get_rendered_template("edna_script.py", variables)
    assert "{{" not in file_content

    assert "dataCollectionId = 123456" in file_content
    assert 'inputFile = "/tmp/autoPROC_input.xml"' in file_content
