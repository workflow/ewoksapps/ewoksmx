from typing import Type

from ewoksmx.models.edml.common import XSData
from ewoksmx.models.edml.xia2_dials import XSDataInputControlXia2DIALS
from ewoksmx.tasks.base_tasks.edna_pipeline import PrepareEdnaPipeline


class XIA2_DIALS_pipeline(PrepareEdnaPipeline, input_names=["XIA2_DIALS"]):
    """Prepare the XIA2 DIALS pipeline.

    https://xia2.github.io/installation.html
    """

    PIPELINE_NAME = "XIA2_DIALS"
    EDNA_PLUGIN_NAME: str = "EDPluginControlXia2DIALSv1_0"
    DATA_MODEL: Type[XSData] = XSDataInputControlXia2DIALS
    SLURM_MEM: int = 96000  # in MB
