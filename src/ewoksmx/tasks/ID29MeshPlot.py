import h5py
import pathlib
import tempfile
import requests
import matplotlib.pyplot as plt

from ewokscore import Task


class ID29MeshPlot(
    Task,
    input_names=["raw_data_path", "callback", "no_columns", "no_rows"],
    optional_input_names=[],
    output_names=["result_path"],
):
    def run(self):
        raw_data_path = pathlib.Path(self.inputs.raw_data_path[0])
        if not raw_data_path.exists():
            raise FileNotFoundError(str(raw_data_path))
        icat_callback_url = self.inputs.callback
        if icat_callback_url is not None:
            requests.put(self.inputs.callback, json={"status": "RUNNING"})
        no_columns = self.get_input_value("no_columns", None)
        no_rows = self.get_input_value("no_rows", None)
        # Find data path
        aggregated_path = raw_data_path / "aggregated"
        list_h5_path = list(aggregated_path.glob("*.h5"))
        first_path = list_h5_path[0]
        first_file_name = first_path.name
        print(first_file_name)
        prefix = "_".join(first_file_name.split("_")[0:-1])
        print(prefix)
        data_path = aggregated_path / (prefix + ".h5")

        f = h5py.File(data_path, "r")
        isHit = f["/entry_0000/processing/peakfinder/isHit"][()]
        isHit.shape = (no_rows, no_columns)

        prefix_without_master = prefix.replace("master_", "")
        tmp_14_days_path = pathlib.Path("/tmp_14_days")
        opid29_path = tmp_14_days_path / "opid29"
        if not opid29_path.exists():
            opid29_path.mkdir(mode=0o755)
        result_dir = pathlib.Path(
            tempfile.mkdtemp(prefix=prefix_without_master + "_", dir=opid29_path)
        )
        result_dir.chmod(0o755)
        result_path = result_dir / "mesh_plot.png"

        plt.imshow(isHit, interpolation="none")
        plt.savefig(result_path)

        requests.put(
            icat_callback_url,
            json={"logs": {"message": f"Result path: {result_path}"}},
        )

        self.outputs.result_path = str(result_path)

        if icat_callback_url is not None:
            requests.put(self.inputs.callback, json={"status": "FINISHED"})
