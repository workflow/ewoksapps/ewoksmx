from typing import Type

from ewoksmx.models.edml.common import XSData
from ewoksmx.models.edml.edna_proc import XSDataEDNAprocInput
from ewoksmx.tasks.base_tasks.edna_pipeline import PrepareEdnaPipeline


class EDNA_proc_pipeline(PrepareEdnaPipeline, input_names=["EDNA_proc"]):
    """Prepare the EDNA_proc pipeline.

    https://www.esrf.fr/UsersAndScience/Experiments/MX/How_to_use_our_beamlines/Run_Your_Experiment/edna-autoprocessing
    """

    PIPELINE_NAME: str = "EDNA_proc"
    EDNA_PLUGIN_NAME: str = "EDPluginControlEDNAprocv1_0"
    DATA_MODEL: Type[XSData] = XSDataEDNAprocInput
