import shutil
import pathlib
import subprocess
from typing import Optional, List, Tuple

from ewoksmx.template import get_rendered_template
from .base_tasks.slurm_pipeline import PrepareSlurmPipeline


class Grenades_fastproc_pipeline(
    PrepareSlurmPipeline, input_names=["grenades_fastproc"]
):
    """Prepare the Grenades pipeline.

    https://www.esrf.fr/home/UsersAndScience/Experiments/MX/How_to_use_our_beamlines/Run_Your_Experiment/grenades.html
    """

    PIPELINE_NAME = "grenades_fastproc"
    CALC_CELL_CMD = "/opt/pxsoft/adp/max/vdefault/debian90/cell_from_space_group.pl"

    def prepare_pipeline(self):
        self.grenades_working_dir.mkdir(parents=True, exist_ok=False, mode=0o755)
        self._create_xds_inp()
        self._modify_xds_inp()
        self._create_dcolid_file()
        self._create_grenades_fastproc_script()

    @property
    def grenades_working_dir(self) -> pathlib.Path:
        return self.processing_base_dir / self.PIPELINE_NAME

    @property
    def xds_inp_path(self) -> pathlib.Path:
        return self.grenades_working_dir / "XDS.INP"

    def _create_xds_inp(self) -> None:
        xds_inp_path = pathlib.Path(self.metadata["xds_inp_path"])
        with open(xds_inp_path, "r") as f:
            lines = f.readlines()

        directory = self.metadata["MX_directory"]
        new_lines = []
        for line in lines:
            if "NAME_TEMPLATE_OF_DATA_FRAMES=" in line:
                template = line.split("/")[-1]
                line = f"   NAME_TEMPLATE_OF_DATA_FRAMES={directory}/{template}\n"
            new_lines.append(line)

        with open(self.xds_inp_path, "w") as f:
            f.write("".join(new_lines))

    def _modify_xds_inp(self) -> None:
        space_group_number = None
        cell_params = None

        forced_spacegroup = self.metadata.get("forced_spacegroup", None)

        if forced_spacegroup is not None:
            self.notify_icat(
                {"step": {"name": self.PIPELINE_NAME, "status": "PREPROCESSING"}}
            )

            space_group_number, cell_params = self._get_spg_and_cell_params(
                forced_spacegroup
            )
            if space_group_number is None or cell_params is None:
                raise RuntimeError(
                    f"Cannot determine cell for space group {forced_spacegroup}"
                )

        exclude_range = self.metadata.get("exclude_range", None)

        if any((space_group_number, cell_params, exclude_range)):
            self._modify_xds_inp_file(
                space_group_number=space_group_number,
                cell_params=cell_params,
                exclude_range=exclude_range,
            )

    def _modify_xds_inp_file(
        self,
        space_group_number: Optional[int] = None,
        cell_params: Optional[List[float]] = None,
        exclude_range: Optional[List[Tuple[int, int]]] = None,
    ):
        if cell_params is not None:
            assert len(cell_params) == 6

        with open(self.xds_inp_path) as f:
            lines = f.readlines()

        new_lines = []
        for line in lines:
            if (
                "SPACE_GROUP_NUMBER" in line
                and space_group_number is not None
                and cell_params is not None
            ):
                line = f"   SPACE_GROUP_NUMBER= {space_group_number}\n"
                space_group_number = None
            elif "UNIT_CELL_CONSTANTS" in line and cell_params is not None:
                line = f"   UNIT_CELL_CONSTANTS= {' '.join(map(str, cell_params))}\n"
                cell_params = None
            new_lines.append(line)

        if space_group_number is not None:
            new_lines.append(f"   SPACE_GROUP_NUMBER= {space_group_number}\n")

        if cell_params is not None:
            line = f"   UNIT_CELL_CONSTANTS= {' '.join(map(str, cell_params))}\n"
            new_lines.append(line)

        if exclude_range is not None:
            for begin, end in exclude_range:
                new_lines.append(f"    EXCLUDE_DATA_RANGE= {begin} {end}\n")

        with open(self.xds_inp_path, "w") as f:
            f.write("".join(new_lines))

    def _get_spg_and_cell_params(
        self, space_group_name: str
    ) -> Tuple[Optional[int], Optional[List[float]]]:
        working_dir = self.grenades_working_dir / "cell_params"
        working_dir.mkdir()

        shutil.copy(self.xds_inp_path, working_dir)

        command_line = [self.CALC_CELL_CMD, "--space_group_name", space_group_name]
        process = subprocess.Popen(
            command_line,
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            cwd=working_dir,
        )
        stdout, stderr = process.communicate()
        stdout = stdout.decode()
        stderr = stderr.decode()
        with open(working_dir / "cell_from_space_group_stdout.txt", "w") as f:
            f.write(stdout)
        with open(working_dir / "cell_from_space_group_stderr.txt", "w") as f:
            f.write(stderr)

        spg_number = None
        cell_params = None
        list_stdout = stdout.split("\n")
        for line in list_stdout:
            if "LATTICE" in line:
                # Convert "LATTICE: 1 77.5 155.8 155.9 60.1 89.9 89.8"
                # to space group number and list of floats
                list_lattice = line.split(":")[1].split()
                spg_number = int(list_lattice[0])
                cell_params = list(map(float, list_lattice[1:]))
                break

        return spg_number, cell_params

    @property
    def dcloid_file_paths(self) -> Tuple[pathlib.Path]:
        return self.grenades_working_dir / "DCOLID.txt", self.icat_dir / "DCOLID.txt"

    def _create_dcolid_file(self) -> None:
        for path in self.dcloid_file_paths:
            with open(path, "w") as f:
                f.write(f"datacollectionID:{self.metadata['MX_dataCollectionId']}\n")

    @property
    def script_file_path(self) -> pathlib.Path:
        return self.processing_base_dir / "grenades_fastproc.sh"

    def _create_grenades_fastproc_script(self) -> None:
        script = get_rendered_template(
            "grenades_fastproc.sh", self.script_template_variables()
        )
        with open(self.script_file_path, mode="w") as f:
            f.write(script)
        self.script_file_path.chmod(0o755)

    def script_template_variables(self) -> dict:
        anomalous = 1
        if "anomalous" in self.metadata and not self.metadata["anomalous"]:
            anomalous = 0
        return {
            "processing_base_dir": self.processing_base_dir,
            "grenades_working_dir": self.grenades_working_dir,
            "MX_dataCollectionId": self.metadata["MX_dataCollectionId"],
            "anomalous": anomalous,
        }
