from typing import Type

from ewoksmx.models.edml.common import XSData
from ewoksmx.models.edml.autoproc import XSDataInputControlAutoPROC
from ewoksmx.tasks.base_tasks.edna_pipeline import PrepareEdnaPipeline


class AutoPROC_pipeline(PrepareEdnaPipeline, input_names=["autoPROC"]):
    """Prepare the AutoPROC pipeline.

    https://www.globalphasing.com/autoproc/
    """

    PIPELINE_NAME: str = "autoPROC"
    EDNA_PLUGIN_NAME: str = "EDPluginControlAutoPROCv1_0"
    DATA_MODEL: Type[XSData] = XSDataInputControlAutoPROC
