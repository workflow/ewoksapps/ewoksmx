import abc
import pathlib
import logging
from typing import Optional

from .icat_task import IcatCallbackTask

logger = logging.getLogger(__name__)


class PrepareSlurmPipeline(
    IcatCallbackTask,
    input_names=["metadata"],
    output_names=["pipeline_name", "slurm_params"],
):
    """Prepare a Slurm pipeline."""

    PIPELINE_NAME: str = NotImplemented
    SLURM_MEM: int = 16000  # in MB
    SLURM_TIME: str = "2:00:00"

    def run(self):
        self._error_message = None
        try:
            pipeline_is_enabled = self.get_input_value(self.PIPELINE_NAME, False)
            err_msg = f"Pipeline '{self.PIPELINE_NAME}' is enabled."
            assert pipeline_is_enabled, err_msg

            self.prepare_pipeline()
        except Exception as e:
            logger.exception(str(e))
            self._error_message = str(e)
            self.notify_icat({"step": {"name": self.PIPELINE_NAME, "status": "ERROR"}})
            self.notify_icat({"logs": {"message": self._error_message}})

        self.outputs.pipeline_name = self.PIPELINE_NAME
        self.outputs.slurm_params = self.slurm_parameters()

    @abc.abstractmethod
    def prepare_pipeline(self):
        pass

    def slurm_parameters(self) -> dict:
        return {
            "script_file_path": str(self.script_file_path),
            "queue": "mx",
            "mem": self.SLURM_MEM,
            "nodes": 1,
            "core": 20,
            "time": self.SLURM_TIME,
            "icat_dir": str(self.icat_dir),
            "icat_callback_url": self._icat_callback_url,
            "no_pipelines": self.metadata["no_pipelines"],
            "pipeline_name": self.PIPELINE_NAME,
            "error_message": self._error_message,
        }

    @property
    def metadata(self) -> dict:
        return self.inputs.metadata

    @property
    def _icat_callback_url(self) -> Optional[str]:
        return self.metadata.get("icat_callback_url", None)

    @property
    def reprocess_path(self) -> pathlib.Path:
        return pathlib.Path(self.metadata["reprocess_path"])

    @property
    def icat_dir(self) -> pathlib.Path:
        return self.reprocess_path / self.PIPELINE_NAME

    @property
    def processing_base_dir(self) -> pathlib.Path:
        return self.icat_dir / "nobackup"

    @property
    @abc.abstractmethod
    def script_file_path(self) -> pathlib.Path:
        pass
