import os
import pathlib
from typing import Type

from ewoksmx.template import get_rendered_template
from ewoksmx.models.edml.common import XSData
from .slurm_pipeline import PrepareSlurmPipeline


class PrepareEdnaPipeline(
    PrepareSlurmPipeline,
    input_names=["metadata"],
    output_names=["pipeline_name", "slurm_params"],
):
    """Prepare an EDNA pipeline."""

    EDNA_PLUGIN_NAME: str = NotImplemented
    DATA_MODEL: Type[XSData] = NotImplemented

    def prepare_pipeline(self) -> None:
        self.processing_base_dir.mkdir(parents=True, exist_ok=False, mode=0o755)
        self._create_edna_input_file()
        self._create_edna_launch_script()

    def _create_edna_input_file(self) -> None:
        # Inject icatProcessDataDir as it's not a part of metadata till now
        model = self.DATA_MODEL(
            **self.metadata,
            icatProcessDataDir=str(self.icat_dir),
            processDirectory=str(self.processing_base_dir),
            ispyb_xml_path=str(self.processing_base_dir / "ispyb.xml"),
        )
        model.to_xml_file(self.input_file_path)

    @property
    def script_file_path(self) -> pathlib.Path:
        return self.processing_base_dir / "edna_script.py"

    def _create_edna_launch_script(self) -> None:
        script = get_rendered_template(
            "edna_script.py", self.script_template_variables()
        )
        with open(self.script_file_path, mode="w") as f:
            f.write(script)
        self.script_file_path.chmod(0o755)

    @property
    def input_file_path(self) -> pathlib.Path:
        return self.processing_base_dir / f"{self.PIPELINE_NAME}_input.xml"

    def script_template_variables(self) -> dict:
        return {
            "beamline": self.metadata["beamline"],
            "proposal": self.metadata["proposal"],
            "scriptDir": self.processing_base_dir,
            "dataCollectionId": self.metadata["MX_dataCollectionId"],
            "inputFile": self.input_file_path,
            "ispybUserName": os.environ.get("ISPyB_user", None),
            "ispybPassword": os.environ.get("ISPyB_pass", None),
            "EDNA_SITE": os.environ.get("EDNA_SITE", None),
            "pluginName": self.EDNA_PLUGIN_NAME,
            "name": self.PIPELINE_NAME,
            "residues": 200,
            "workingDir": self.processing_base_dir,
        }
