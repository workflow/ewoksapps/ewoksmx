import logging
from typing import Optional

import requests
from ewokscore import Task

logger = logging.getLogger(__name__)


class IcatCallbackTask(Task):
    """Task with ICAT callback."""

    @property
    def _icat_callback_url(self) -> Optional[str]:
        pass

    def notify_icat(self, data: dict) -> None:
        if self._icat_callback_url:
            logger.info("Send notification to ICAT: %s", data)
            requests.put(self._icat_callback_url, json=data)
        else:
            logger.info("Skip notification to ICAT (no callback URL): %s", data)
