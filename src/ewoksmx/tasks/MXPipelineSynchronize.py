import json

import requests
from ewokscore import Task


class MXPipelineSynchronize(
    Task,
    input_names=["slurm_params"],
    optional_input_names=[],
    output_names=["slurm_params"],
):
    def run(self):
        slurm_params = self.inputs.slurm_params
        response = requests.get(slurm_params["icat_callback_url"])
        json_message = response.text

        dict_message = json.loads(json_message)

        job_id = self.job_id

        no_finished = 0
        for dict_investigation in dict_message:
            if "jobId" in dict_investigation:
                if dict_investigation["jobId"] == job_id:
                    for dict_step in dict_investigation["steps"]:
                        if dict_step["status"] in ["FINISHED", "ERROR"]:
                            no_finished += 1

        if self.inputs.slurm_params["no_pipelines"] == no_finished:
            requests.put(slurm_params["icat_callback_url"], json={"status": "FINISHED"})
            slurm_params["workflow_status"] = "FINISHED"

        self.outputs.slurm_params = slurm_params
